﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	
	[SerializeField]
	private GameObject playerLifeBar;
	[SerializeField]
	private Text enemyScoreText;
	private Slider lifeSlider; 

	private Animator animator;

	public int startHealth = 5;
	private int health;

	public float moveSpeed = 3;
	public float rotSpeed = 12;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		lifeSlider = playerLifeBar.GetComponent<Slider> ();
		health = startHealth;

		if (PlayerPrefs.HasKey ("enemyScoreText")) {
			enemyScoreText.text = PlayerPrefs.GetInt("enemyScoreText").ToString();
		}
	}

	
	// Update is called once per frame
	void Update () {

		Vector3 movement = Vector3.zero;
		float horInput = Input.GetAxis ("Horizontal");
		float vertInput = Input.GetAxis ("Vertical");

		animator.SetFloat ("Moving", vertInput);

		if ( (horInput != 0 || vertInput != 0) ) {
		transform.position += transform.right * vertInput * moveSpeed * Time.deltaTime;
			if (vertInput >= 0) {
				transform.Rotate (0, 0, -horInput * (rotSpeed * 100) * Time.deltaTime);
			} else {
				transform.Rotate (0, 0, horInput * (rotSpeed * 50) * Time.deltaTime);

			}
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Bullet"){
			lifeSlider.value = --health;
			if (health <= 0) {
				RestartScene ();
			}
		}
	}


	private void RestartScene(){
		//health = startHealth;
	//	lifeSlider.value = startHealth;
		//enemyScoreText.text = (int.Parse (enemyScoreText.text)+1).ToString();




		PlayerPrefs.SetInt("enemyScoreText", (int.Parse (enemyScoreText.text)) + 1);
		StartCoroutine (Wait(2));

	}


	private IEnumerator Wait(float duration)
	{

		AudioSource[] allAudioSources;
		allAudioSources = FindObjectsOfType<AudioSource> () as AudioSource[];
		foreach (AudioSource audio in allAudioSources) {
			audio.Stop ();
		}

		yield return new WaitForSeconds(duration);   //Wait
		SceneManager.LoadScene (1);

	} 


}