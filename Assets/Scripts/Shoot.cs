﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {
	[SerializeField]
	private GameObject bulletPrefab;
	private GameObject bullet;
	[SerializeField]
	private GameObject player;
	[SerializeField]
	private GameObject enemy;

	public float speed = 6f;

	public bool canShoot = false;

	private float shotTime = 0f;
	private float shotInterval = 1f;

	private Animator playerAnimator;
	private Animator enemyAnimator;

	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
		playerAnimator = player.GetComponent<Animator> ();
		enemyAnimator = enemy.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (tag == "PlayerGun") {

			if (Input.GetKeyDown (KeyCode.LeftControl)) {
				StartShoot ();
				audioSource.Play ();
				playerAnimator.SetTrigger ("Shoot");
			}
		}

		if (tag == "EnemyGun") {

			if (canShoot && (Time.time - shotTime) > shotInterval) {
				shotInterval = Random.Range (0.5f, 1f);

				StartShoot ();
				audioSource.Play ();
				enemyAnimator.SetTrigger ("Shoot");
			}

		}
	}

	public void StartShoot(){
		shotTime = Time.time;
		bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
		Rigidbody2D bulletRb = bullet.GetComponent<Rigidbody2D> ();
		bulletRb.AddForce(transform.right * speed * Time.deltaTime);
	}

}
