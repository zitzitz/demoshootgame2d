﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	[SerializeField]
	private GameObject menuButton;
	[SerializeField]
	private GameObject shootVolumeScrollBar;
	[SerializeField]
	private GameObject musicVolumeScrollBAr;
	/*[SerializeField]
	private GameObject player;
	[SerializeField]
	private GameObject enemy;*/


	void Awake(){
		Time.timeScale = 1;



		if(PlayerPrefs.HasKey("volume") ){
			GameObject.FindWithTag ("Sound").GetComponent<AudioSource> ().volume = PlayerPrefs.GetFloat("volume");
			musicVolumeScrollBAr.GetComponent<Scrollbar> ().value = PlayerPrefs.GetFloat("volume");

		}

		if (PlayerPrefs.HasKey ("GunVolume")) {
			GameObject.FindWithTag ("PlayerGun").GetComponent<AudioSource> ().volume = PlayerPrefs.GetFloat("GunVolume");
			GameObject.FindWithTag ("EnemyGun").GetComponent<AudioSource> ().volume = PlayerPrefs.GetFloat("GunVolume");
			shootVolumeScrollBar.GetComponent<Scrollbar> ().value = PlayerPrefs.GetFloat("GunVolume");
		}
			
	}


	public void PlayGame() {
		SceneManager.LoadScene (1);
	}


	public void MusicVolume(float volume){
		GameObject.FindWithTag ("Sound").GetComponent<AudioSource> ().volume = volume;
		PlayerPrefs.SetFloat ("volume", volume);
	}

	public void ShootVolume(float volume){
		GameObject.FindWithTag ("PlayerGun").GetComponent<AudioSource> ().volume = volume;
		GameObject.FindWithTag ("EnemyGun").GetComponent<AudioSource> ().volume = volume;

		PlayerPrefs.SetFloat ("GunVolume", volume);

	}

	void Update(){
		
	}


	public void QuitGame() {
		
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif
	}

	public void OnMenuButtonClick(){

		if (Time.timeScale == 0)
			Time.timeScale = 1;
		else
			Time.timeScale = 0;
		
	}

	public void OnMenuButtonEnter(){
		menuButton.transform.localScale = new Vector3 (1.2f, 1.2f, 1f);
	}

	public void OnMenuButtonExit(){
		menuButton.transform.localScale = new Vector3 (1f, 1f, 1f);

	}

	public void ToMainMenu(){
		PlayerPrefs.DeleteKey ("enemyScoreText");
		PlayerPrefs.DeleteKey ("playerScoreText");
		SceneManager.LoadScene (0);

	}

	void OnApplicationQuit(){
		PlayerPrefs.DeleteKey ("enemyScoreText");
		PlayerPrefs.DeleteKey ("playerScoreText");

	}

}
