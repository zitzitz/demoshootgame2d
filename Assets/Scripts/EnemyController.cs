﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour {

	[SerializeField]
	private Transform player;
	[SerializeField]
	private Transform[] moveSpots;
	[SerializeField]
	private GameObject enemyLifeBar;
	private Slider lifeSlider;

	[SerializeField]
	private Text playerScoreText;

	public float speed = 6f;
	public float patrolSpeed = 4f;

	public int startHealth = 5;
	private int health;

	private int randomSpot;
	private float weitTime;
	public float startWeitTime = 3;


	private Vector2 evPosition;

	void Start () {

		health = startHealth;

		weitTime = startWeitTime;
		randomSpot = Random.Range (0, moveSpots.Length);

		lifeSlider = enemyLifeBar.GetComponent<Slider> ();

		if (PlayerPrefs.HasKey ("playerScoreText")) {
			playerScoreText.text = PlayerPrefs.GetInt("playerScoreText").ToString();
		}
	}
	
	void Update () {
		
		if (Vector2.Distance (player.position, transform.position) < 10 && Vector2.Distance (player.position, transform.position) > 5) {
			GameObject.FindWithTag ("EnemyGun").GetComponent<Shoot> ().canShoot = false;
			LookTo (player);
			transform.position = Vector2.MoveTowards (transform.position, player.position, speed* Time.deltaTime);
		}else 
		if (Vector2.Distance (player.position, transform.position) <= 5) {
				
			LookTo (player);
			GameObject.FindWithTag ("EnemyGun").GetComponent<Shoot> ().canShoot = true;
		} else {
				
			GameObject.FindWithTag ("EnemyGun").GetComponent<Shoot> ().canShoot = false;
			patrol ();

		} 
		
	}

	//
	private bool PlayerRedyShoot(){
		float checkAngle = Mathf.Min(2,359.9999f) / 2;

		float dot = Vector3.Dot(player.right, (transform.position - player.position).normalized); 

		float viewAngle = (1 - dot) * 90; 

		if (viewAngle <= checkAngle)
			return true;
		else
			return false;
	}

	private void LookTo(Transform target){
		Vector3 dir = target.position - transform.position;
		float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

	}

	private void patrol(){
		
		LookTo (moveSpots [randomSpot]);
		transform.position = Vector2.MoveTowards (transform.position, moveSpots [randomSpot].position, patrolSpeed * Time.deltaTime);

		if(Vector2.Distance(transform.position,moveSpots[randomSpot].position) < 0.2f) {
			if(weitTime <= 0){
				
				if(weitTime <= 0) {
					
					randomSpot = Random.Range (0, moveSpots.Length);
					//maybe better use Random
					weitTime = startWeitTime;

				} else  {
					
					weitTime -= Time.deltaTime;

				} 
			}
		}	

	}


	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Bullet"){
			lifeSlider.value = --health;
			Destroy (col.gameObject);
			if (health <= 0) {
				RestartScene ();
			}
		}
	}

	private void RestartScene(){
		PlayerPrefs.SetInt("playerScoreText", (int.Parse (playerScoreText.text)) + 1);
		SceneManager.LoadScene (1);
	}
}
